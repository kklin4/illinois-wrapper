var http = require("http");

var default_options = {
    hostname: "localhost",
    port: 5025,
    headers: {
        "content-type": "application/json",
    },
};

var greetings = function (methodType, path_URL, args, callback) {
    var options = default_options;
    options["method"] = methodType;
    options["path"] = path_URL;

    var request = http.request(options, function (result) {
		console.log("Status code: " + result.statusCode);
		if (result.statusCode < 200 || result.statusCode > 299) {
			return callback(null, result.statusCode);
		}
		
        result.setEncoding("UTF-8");

        var resultBody = "";
        result.on("data", function (chunk) {
            resultBody += chunk;
        });

        result.on("end", function() {
            console.log(resultBody);
			try {
				return callback(JSON.parse(resultBody), result.statusCode);
			} catch (error) {
				return callback(null, result.statusCode);
			}
        });
    });

    request.on("error", function (error) {
        console.log("problem with request: " + error.message);
    });

    if (args !== null) {
		console.log(args);
        request.write(JSON.stringify(args), 'utf8');
    }

    request.end();
};

var arguments = {
	"stringA": "Apple",
	"stringB": "Orange",
	"similarityTechnique": "_25_cbow_binary"
}

greetings("POST", "/embedding_sim", arguments, function(resultBody, statusCode) {
    console.log("done");
});




