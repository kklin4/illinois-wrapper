package edu.illinois.cs.cogcomp.config;

import edu.illinois.cs.cogcomp.core.utilities.configuration.Configurator;
import edu.illinois.cs.cogcomp.core.utilities.configuration.Property;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.common.EmbeddingSimConstants;

public class EmbeddingSimConfigurator extends Configurator{
	
	public final Property PARAGRAM_PATH_WS = new Property(EmbeddingSimConstants.PARAGRAMPATHWS, "paragram-data-ws/paragram_300_ws353.txt");
	public final Property PARAGRAM_PATH_SL = new Property(EmbeddingSimConstants.PARAGRAMPATHSL, "paragram-data-sl/paragram_300_sl999.txt");
	
	//public final Property EVAL_WORD_PATH_CBOW = new Property("evalWordPathCBOW", "eval-word-data-cbow");
	
	public final Property EVAL_WORD_PATH_25_CBOW_BINARY = new Property(EmbeddingSimConstants.EVALWORDPATH25CBOWBINARY, "eval-word-data-cbow/25_cbow_binary.txt");
	public final Property EVAL_WORD_PATH_25_CBOW_4 = new Property(EmbeddingSimConstants.EVALWORDPATH25CBOW4, "eval-word-data-cbow/25_cbow_4.txt");
	public final Property EVAL_WORD_PATH_25_CBOW_6 = new Property(EmbeddingSimConstants.EVALWORDPATH25CBOW6, "eval-word-data-cbow/25_cbow_6.txt");
	public final Property EVAL_WORD_PATH_25_CBOW_8 = new Property(EmbeddingSimConstants.EVALWORDPATH25CBOW8, "eval-word-data-cbow/25_cbow_8.txt");
	public final Property EVAL_WORD_PATH_200_CBOW_BINARY = new Property(EmbeddingSimConstants.EVALWORDPATH200CBOWBINARY, "eval-word-data-cbow/200_cbow_binary.txt");
	public final Property EVAL_WORD_PATH_200_CBOW_4 = new Property(EmbeddingSimConstants.EVALWORDPATH200CBOW4, "eval-word-data-cbow/200_cbow_4.txt");
	public final Property EVAL_WORD_PATH_200_CBOW_6 = new Property(EmbeddingSimConstants.EVALWORDPATH200CBOW6, "eval-word-data-cbow/200_cbow_6.txt");
	public final Property EVAL_WORD_PATH_200_CBOW_8 = new Property(EmbeddingSimConstants.EVALWORDPATH200CBOW8, "eval-word-data-cbow/200_cbow_8.txt");

	//public final Property EVAL_WORD_PATH_SKIP = new Property("evalWordPathSKIP", "eval-word-data-skip");
	
	public final Property EVAL_WORD_PATH_25_SKIP_BINARY = new Property(EmbeddingSimConstants.EVALWORDPATH25SKIPBINARY, "eval-word-data-skip/25_skip_binary.txt");
	public final Property EVAL_WORD_PATH_25_SKIP_4 = new Property(EmbeddingSimConstants.EVALWORDPATH25SKIP4, "eval-word-data-skip/25_skip_4.txt");
	public final Property EVAL_WORD_PATH_25_SKIP_6 = new Property(EmbeddingSimConstants.EVALWORDPATH25SKIP6, "eval-word-data-skip/25_skip_6.txt");
	public final Property EVAL_WORD_PATH_25_SKIP_8 = new Property(EmbeddingSimConstants.EVALWORDPATH25SKIP8, "eval-word-data-skip/25_skip_8.txt");
	public final Property EVAL_WORD_PATH_200_SKIP_BINARY = new Property(EmbeddingSimConstants.EVALWORDPATH200SKIPBINARY, "eval-word-data-skip/200_skip_binary.txt");
	public final Property EVAL_WORD_PATH_200_SKIP_4 = new Property(EmbeddingSimConstants.EVALWORDPATH200SKIP4, "eval-word-data-skip/200_skip_4.txt");
	public final Property EVAL_WORD_PATH_200_SKIP_6 = new Property(EmbeddingSimConstants.EVALWORDPATH200SKIP6, "eval-word-data-skip/200_skip_6.txt");
	public final Property EVAL_WORD_PATH_200_SKIP_8 = new Property(EmbeddingSimConstants.EVALWORDPATH200SKIP8, "eval-word-data-skip/200_skip_8.txt");

	public final Property EN_WIKI_TEXT_200 = new Property(EmbeddingSimConstants.ENWIKITEXTPATH200, "enwikitext-data/enwikitext_vivek200.txt");
	public final Property EN_WIKI_TEXT_200_1 = new Property(EmbeddingSimConstants.ENWIKITEXTPATH200_1, "en-wiki-text-data-1/en_wiki_text_vivek200_1.txt");
	public final Property EN_WIKI_TEXT_200_2 = new Property(EmbeddingSimConstants.ENWIKITEXTPATH200_2, "en-wiki-text-data-2/en_wiki_text_vivek200_2.txt");
	
	@Override
	public ResourceManager getDefaultConfig() {
		
		Property[] props = {
				PARAGRAM_PATH_WS, PARAGRAM_PATH_SL,
				EVAL_WORD_PATH_25_CBOW_BINARY, EVAL_WORD_PATH_25_CBOW_4, EVAL_WORD_PATH_25_CBOW_6, EVAL_WORD_PATH_25_CBOW_8, 
				EVAL_WORD_PATH_200_CBOW_BINARY, EVAL_WORD_PATH_200_CBOW_4, EVAL_WORD_PATH_200_CBOW_6, EVAL_WORD_PATH_200_CBOW_8, 
				EVAL_WORD_PATH_25_SKIP_BINARY, EVAL_WORD_PATH_25_SKIP_4, EVAL_WORD_PATH_25_SKIP_6, EVAL_WORD_PATH_25_SKIP_8, 
				EVAL_WORD_PATH_200_SKIP_BINARY, EVAL_WORD_PATH_200_SKIP_4, EVAL_WORD_PATH_200_SKIP_6, EVAL_WORD_PATH_200_SKIP_8,
				EN_WIKI_TEXT_200, EN_WIKI_TEXT_200_1, EN_WIKI_TEXT_200_2
		};

        return new ResourceManager(generateProperties(props));
	}
	
}
