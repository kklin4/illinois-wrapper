package edu.illinois.cs.cogcomp.embedding_sim;

import edu.illinois.cs.cogcomp.bigdata.mapdb.MapDB;
import edu.illinois.cs.cogcomp.common.EmbeddingSimConstants;
import edu.illinois.cs.cogcomp.common.EmbeddingSimMath;
import edu.illinois.cs.cogcomp.config.EmbeddingSimConfigurator;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.sim.WNSim;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class EmbeddingSim {

    public static enum SimilarityTechnique {
        WNSim,
        _25_cbow_binary, _25_cbow_4, _25_cbow_6, _25_cbow_8,
        _200_cbow_binary, _200_cbow_4, _200_cbow_6, _200_cbow_8,
        _25_skip_binary, _25_skip_4, _25_skip_6, _25_skip_8,
        _200_skip_binary, _200_skip_4, _200_skip_6, _200_skip_8,
        paragram_300_ws353, paragram_300_sl999,
        en_wiki_text_vivek_200,
    }

	public static ResourceManager resourceManager;
    //public static HashMap<SimilarityTechnique, String> embeddingDataPaths;
    public static HashMap<SimilarityTechnique, String[]> embeddingDataPaths;
    public static WNSim wnsim;
    static {
    	resourceManager = new EmbeddingSimConfigurator().getDefaultConfig();
        embeddingDataPaths = new HashMap<>();

        String[] paragram_300_ws353_paths = {resourceManager.getString(EmbeddingSimConstants.PARAGRAMPATHWS)};
        embeddingDataPaths.put(SimilarityTechnique.paragram_300_ws353, paragram_300_ws353_paths);
        String[] paragram_300_sl999_paths = {resourceManager.getString(EmbeddingSimConstants.PARAGRAMPATHSL)};
        embeddingDataPaths.put(SimilarityTechnique.paragram_300_sl999, paragram_300_sl999_paths);

        String[] _25_cbow_binary_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH25CBOWBINARY)};
        embeddingDataPaths.put(SimilarityTechnique._25_cbow_binary, _25_cbow_binary_paths);
        String[] _25_cbow_4_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH25CBOW4)};
        embeddingDataPaths.put(SimilarityTechnique._25_cbow_4, _25_cbow_4_paths);
        String[] _25_cbow_6_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH25CBOW6)};
        embeddingDataPaths.put(SimilarityTechnique._25_cbow_6, _25_cbow_6_paths);
        String[] _25_cbow_8_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH25CBOW8)};
        embeddingDataPaths.put(SimilarityTechnique._25_cbow_8, _25_cbow_8_paths);

        String[] _200_cbow_binary_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH200CBOWBINARY)};
        embeddingDataPaths.put(SimilarityTechnique._200_cbow_binary, _200_cbow_binary_paths);
        String[] _200_cbow_4_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH200CBOW4)};
        embeddingDataPaths.put(SimilarityTechnique._200_cbow_4, _200_cbow_4_paths);
        String[] _200_cbow_6_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH200CBOW6)};
        embeddingDataPaths.put(SimilarityTechnique._200_cbow_6, _200_cbow_6_paths);
        String[] _200_cbow_8_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH200CBOW8)};
        embeddingDataPaths.put(SimilarityTechnique._200_cbow_8, _200_cbow_8_paths);

        String[] _25_skip_binary_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH25SKIPBINARY)};
        embeddingDataPaths.put(SimilarityTechnique._25_skip_binary, _25_skip_binary_paths);
        String[] _25_skip_4_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH25SKIP4)};
        embeddingDataPaths.put(SimilarityTechnique._25_skip_4, _25_skip_4_paths);
        String[] _25_skip_6_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH25SKIP6)};
        embeddingDataPaths.put(SimilarityTechnique._25_skip_6, _25_skip_6_paths);
        String[] _25_skip_8_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH25SKIP8)};
        embeddingDataPaths.put(SimilarityTechnique._25_skip_8, _25_skip_8_paths);

        String[] _200_skip_binary_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH200SKIPBINARY)};
        embeddingDataPaths.put(SimilarityTechnique._200_skip_binary, _200_skip_binary_paths);
        String[] _200_skip_4_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH200SKIP4)};
        embeddingDataPaths.put(SimilarityTechnique._200_skip_4, _200_skip_4_paths);
        String[] _200_skip_6_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH200SKIP6)};
        embeddingDataPaths.put(SimilarityTechnique._200_skip_6, _200_skip_6_paths);
        String[] _200_skip_8_paths = {resourceManager.getString(EmbeddingSimConstants.EVALWORDPATH200SKIP8)};
        embeddingDataPaths.put(SimilarityTechnique._200_skip_8, _200_skip_8_paths);

        String[] en_wiki_text_vivek_200_paths = {resourceManager.getString(EmbeddingSimConstants.ENWIKITEXTPATH200_1), resourceManager.getString(EmbeddingSimConstants.ENWIKITEXTPATH200_2)};
        embeddingDataPaths.put(SimilarityTechnique.en_wiki_text_vivek_200, en_wiki_text_vivek_200_paths);
        
        wnsim = null;
    }
    
    private SimilarityTechnique similarityTechnique;
    public Map<String, Double[]> embeddingMap;//TODO private & getter
    public EmbeddingSim(SimilarityTechnique similarityTechnique) {
    	this.similarityTechnique = similarityTechnique;
    	
    	if (similarityTechnique != SimilarityTechnique.WNSim) {
    		//String mapName = "embedding_sim_cache";
        	String mapName = similarityTechnique.toString();
            String mapPath = "data/embeddingSimResponseCache" + mapName + "/";
        	
    		embeddingMap = MapDB.newDefaultDb(mapPath, mapName).make().getHashMap(mapName);
    		if (embeddingMap.size() == 0) {
        		try {
                    String[] embedding_data_paths = embeddingDataPaths.get(similarityTechnique);
                    for (int p=0; p<embedding_data_paths.length; p++) {
                        String embeddingDataPath = embedding_data_paths[p];

                        InputStream inputStream = ClassLoader.getSystemResourceAsStream(embeddingDataPath);
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        int count=0;
                        String thisLine = null;
                        if (similarityTechnique == SimilarityTechnique.en_wiki_text_vivek_200) {
                            thisLine = bufferedReader.readLine();//remove 1 to 3616574
                        }
                        while ((thisLine = bufferedReader.readLine()) != null) {
                            String[] thisLineSplit = thisLine.split(" ");//COULDDO: more efficient
                            Double[] thisEmbedding = new Double[thisLineSplit.length - 1];
                            for (int i=0; i<thisEmbedding.length; i++) {
                                thisEmbedding[i] = Double.parseDouble(thisLineSplit[i+1]);
                            }
                            embeddingMap.put(thisLineSplit[0], thisEmbedding);

//                            count++;
//                            if (count%10000 == 0) {
//                                System.out.println(count);
//                            }
                        }
                    }
        		} catch (IOException e) {
        			e.printStackTrace();
        		}
    		}
    		System.out.println("Response cache " + mapName + " has " + embeddingMap.size() + " items. ");
    	} else {//if == SimilarityTechnique.WNSim
    		if (wnsim == null) {
    	        try {
    	            wnsim = new WNSim();
    	        } catch (IOException exception) {
    	            exception.printStackTrace();
    	        }
    		}
    	}
    }

    private static int n;
    public static String[] word_data;

    public static void main (String[] args) {

        int n=5000;
        word_data = new String[n];

        try(BufferedReader br = new BufferedReader(new FileReader("small_data/word_frequency_data/word_frequency_data.csv"))) {
//            for(String line; (line = br.readLine()) != null; ) {
//
//            }
            System.out.println(br.readLine());
            for (int i=0; i<n; i++) {
                String line = br.readLine();
                //System.out.println(line);
                word_data[i] = line.split(",")[1];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Start Tests");

        test0();
        //test1();
        //test2();
        //test3();
        //test4();

        System.out.println("Done");
    }

    private static void test0 () {
        System.out.println("test0 start");

        String stringA;
        String stringB;

        System.out.println(System.currentTimeMillis());

        EmbeddingSim embeddingSim25CBOW4 = new EmbeddingSim(SimilarityTechnique._25_cbow_4);//was already filled before timing
        System.out.println(System.currentTimeMillis());

        System.out.println();

        stringA = "man";
        stringB = "woman";
        System.out.println("similarity('" + stringA + "', '" + stringB + "')");

        System.out.println(System.currentTimeMillis());

        embeddingSim25CBOW4.similarity(stringA, stringB);
        System.out.println(System.currentTimeMillis());
        embeddingSim25CBOW4.similarity(stringA, stringB);
        System.out.println(System.currentTimeMillis());

        System.out.println("test0 stop");
    }

    private static void test1 () {
        System.out.println("test1 start");

        String stringA;
        String stringB;

        try {
            System.setOut(new PrintStream(new FileOutputStream("test1.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(System.currentTimeMillis());

        EmbeddingSim embeddingSim25CBOW4 = new EmbeddingSim(SimilarityTechnique._25_cbow_4);//was already filled before timing
        System.out.println(System.currentTimeMillis());

        System.out.println();

        stringA = "man";
        stringB = "woman";
        System.out.println("similarity('" + stringA + "', '" + stringB + "')");
        for (int t=0; t<100; t++) {
            embeddingSim25CBOW4.similarity(stringA, stringB);
            System.out.println(System.currentTimeMillis());
        }
        System.out.println();

        stringA = "king";
        stringB = "queen";
        System.out.println("similarity('" + stringA + "', '" + stringB + "')");
        for (int t=0; t<100; t++) {
            embeddingSim25CBOW4.similarity(stringA, stringB);
            System.out.println(System.currentTimeMillis());
        }
        System.out.println();

        stringA = "cat";
        stringB = "dog";
        System.out.println("similarity('" + stringA + "', '" + stringB + "')");
        for (int t=0; t<100; t++) {
            embeddingSim25CBOW4.similarity(stringA, stringB);
            System.out.println(System.currentTimeMillis());
        }
        System.out.println();

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        System.out.println("test1 stop");
    }

    private static void test2 () {
        System.out.println("test2 start");

        try {
            System.setOut(new PrintStream(new FileOutputStream("test2.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(System.currentTimeMillis());

        EmbeddingSim embeddingSim25CBOW2 = new EmbeddingSim(SimilarityTechnique._25_cbow_binary);//was already filled before timing
        System.out.println(System.currentTimeMillis());

        for (int i=0; i<n; i+=2) {
            System.out.println(embeddingSim25CBOW2.similarity(word_data[i], word_data[i+1]));
        }
        System.out.println(System.currentTimeMillis());

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        System.out.println("test2 stop");
    }

    private static void test3 () {
        System.out.println("test3 start");

        try {
            System.setOut(new PrintStream(new FileOutputStream("test3.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(System.currentTimeMillis());

        EmbeddingSim embeddingSim25SKIP2 = new EmbeddingSim(SimilarityTechnique._25_skip_binary);//was already filled before timing
        System.out.println(System.currentTimeMillis());

        for (int i=0; i<n; i+=2) {
            System.out.println(embeddingSim25SKIP2.similarity(word_data[i], word_data[i+1]));
        }
        System.out.println(System.currentTimeMillis());

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        System.out.println("test3 stop");
    }

    private static void test4 () {
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        System.out.println("test4 start");

        try {
            System.setOut(new PrintStream(new FileOutputStream("test4.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(System.currentTimeMillis());

        EmbeddingSim embeddingSim25CBOW2 = new EmbeddingSim(SimilarityTechnique._25_cbow_binary);//was already filled before timing
        System.out.println(System.currentTimeMillis());
        EmbeddingSim embeddingSim25SKIP2 = new EmbeddingSim(SimilarityTechnique._25_skip_binary);//was already filled before timing
        System.out.println(System.currentTimeMillis());

        for (int i=0; i<n; i+=2) {
            System.out.println(embeddingSim25CBOW2.similarity(word_data[i], word_data[i+1]));
            System.out.println(embeddingSim25SKIP2.similarity(word_data[i], word_data[i+1]));
        }
        System.out.println(System.currentTimeMillis());

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        System.out.println("test4 stop");
    }

    public double similarity(String stringA, String stringB) {
        if (similarityTechnique != SimilarityTechnique.WNSim) {
            return cosine_similarity(stringA, stringB);
        } else {
        	return wnsim.compare(stringA, stringB).score;
        }
    }

    public double cosine_similarity(String stringA, String stringB) {

        stringA = stringA.toLowerCase();//TODO: recheck this assumption
        stringB = stringB.toLowerCase();

        Double[] embeddingA = embeddingMap.get(stringA);
        Double[] embeddingB = embeddingMap.get(stringB);

        if (embeddingA == null || embeddingB == null) {
            return 0.0;
        } else {
            return EmbeddingSimMath.cosine_similarity(embeddingA, embeddingB);
        }
    }

    public SimilarityTechnique getSimilarityTechnique() {
        return this.similarityTechnique;
    }
}
