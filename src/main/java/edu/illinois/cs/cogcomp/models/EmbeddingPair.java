package edu.illinois.cs.cogcomp.models;

public class EmbeddingPair {

    private String stringA;
    private String stringB;
    private String similarityTechnique;
    private double similarity;

    public String getStringA() {
        return stringA;
    }
    public void setStringA(String stringA) {
        this.stringA = stringA;
    }

    public String getStringB() {
        return stringB;
    }
    public void setStringB(String stringB) {
        this.stringB = stringB;
    }

    public String getSimilarityTechnique() {
        return similarityTechnique;
    }
    public void setSimilarityTechnique(String similarityTechnique) {
        this.similarityTechnique = similarityTechnique;
    }

    public double getSimilarity() {
        return similarity;
    }
    public void setSimilarity(double similarity) {
        this.similarity = similarity;
    }
}
