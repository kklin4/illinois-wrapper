package edu.illinois.cs.cogcomp.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import edu.illinois.cs.cogcomp.embedding_sim.EmbeddingSim;
import edu.illinois.cs.cogcomp.models.EmbeddingPair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class EmbeddingSimConnection {

    public static ObjectMapper objectMapper;
    private static String URL;

    static {
        objectMapper = new ObjectMapper();
        URL = "http://82402122.ngrok.io/embedding_sim/";
        //URL = "http://localhost:5025/embedding_sim/";
        //URL = "http://192.17.58.156:5025/embedding_sim/";
    }

    public static void main(String[] args) {

        System.out.println("apples, oranges, " + EmbeddingSim.SimilarityTechnique.paragram_300_sl999.toString());
        EmbeddingPair embeddingApplesOranges = EmbeddingSimGET("apples", "oranges", EmbeddingSim.SimilarityTechnique.paragram_300_sl999.toString());
        System.out.println(embeddingApplesOranges.getSimilarity());

        System.out.println("apples, oranges, " + EmbeddingSim.SimilarityTechnique.paragram_300_ws353.toString());
        EmbeddingPair embeddingApplesOranges2 = EmbeddingSimGET("apples", "oranges", EmbeddingSim.SimilarityTechnique.paragram_300_ws353.toString());
        System.out.println(embeddingApplesOranges2.getSimilarity());
    }

    public static EmbeddingPair EmbeddingSimGET(String stringA, String stringB, EmbeddingSim.SimilarityTechnique similarityTechnique) {
        return EmbeddingSimGET(stringA, stringB, similarityTechnique.toString());
    }

    public static EmbeddingPair EmbeddingSimGET(String stringA, String stringB, String similarityTechnique) {
        EmbeddingPair embeddingPair = null;
        try {
            URL url = new URL(URL + stringA + "/" + stringB + "/" + similarityTechnique + "/");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
//            System.out.println("GET Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK || (responseCode >= 200 && responseCode < 300)) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                StringBuffer response = new StringBuffer();
                for (String inputLine = in.readLine(); inputLine != null; inputLine = in.readLine()) {
                    response.append(inputLine);
                }

                in.close();

                String responseString = response.toString();
//                System.out.println(responseString);

                embeddingPair = objectMapper.readValue(responseString, EmbeddingPair.class);
            } else {
                System.out.println("GET request failed");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return embeddingPair;
    }


    public static EmbeddingPair EmbeddingSimPOST(String stringA, String stringB, EmbeddingSim.SimilarityTechnique similarityTechnique) {
        return EmbeddingSimPOST(stringA, stringB, similarityTechnique.toString());
    }

    //this is actually a get method, but I wanted to see that a post request actually works
    //also, if the word itself has a slash (e.g. "and/or"), then the get method thinks it is 2 different arguments
    public static EmbeddingPair EmbeddingSimPOST(String stringA, String stringB, String similarityTechnique) {
        EmbeddingPair embeddingPair = null;
        try {
            URL url = new URL(URL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/json;charset=utf8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Method", "POST");

            JsonObject jsonObject = new JsonObject();//http://stackoverflow.com/questions/22566433/http-415-unsupported-media-type-error-with-json
            jsonObject.addProperty("stringA", stringA);
            jsonObject.addProperty("stringB", stringB);
            jsonObject.addProperty("similarityTechnique", similarityTechnique);

            connection.setDoOutput(true);
            connection.setDoInput(true);
            OutputStream os = connection.getOutputStream();
            os.write(jsonObject.toString().getBytes("UTF-8"));
            os.close();

            int responseCode = connection.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK || (responseCode >= 200 && responseCode < 300)) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                StringBuffer response = new StringBuffer();
                for (String inputLine = in.readLine(); inputLine != null; inputLine = in.readLine()) {
                    response.append(inputLine);
                }

                in.close();

                String responseString = response.toString();
                System.out.println(responseString);

                embeddingPair = objectMapper.readValue(responseString, EmbeddingPair.class);
            } else {
                System.out.println("POST request failed");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return embeddingPair;
    }
}
