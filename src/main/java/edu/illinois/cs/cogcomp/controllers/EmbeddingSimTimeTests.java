package edu.illinois.cs.cogcomp.controllers;

import edu.illinois.cs.cogcomp.embedding_sim.EmbeddingSim;
import edu.illinois.cs.cogcomp.models.EmbeddingPair;

import java.io.*;

public class EmbeddingSimTimeTests {// see http://www.journaldev.com/7148/java-httpurlconnection-example-java-http-request-get-post

    private static String URL;
    private static int n;
    public static String[] word_data;

    public static void main(String[] args) {

        //URL = "http://localhost:5025/embedding_sim/";
        URL = "http://192.17.58.156:5025/embedding_sim/";

        n=5000;
        word_data = new String[n];

        try(BufferedReader br = new BufferedReader(new FileReader("small_data/word_frequency_data/word_frequency_data.csv"))) {
            System.out.println(br.readLine());
            for (int i=0; i<n; i++) {
                String line = br.readLine();
                word_data[i] = line.split(",")[1];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        //test0();
        //test1();

        //test2();
        //test3();
        //test4();

        //test5();
        test6();





//        try {
//            URL url = new URL("http://localhost:5025/");
//            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
//
//            for (String chunk = br.readLine(); chunk != null; chunk = br.readLine()) {
//                System.out.println(chunk);
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    public static void test0() {
        System.out.println("test0 start");

        System.out.println(System.currentTimeMillis());

        EmbeddingPair embeddingAppleOrange = EmbeddingSimConnection.EmbeddingSimGET("apple", "orange", "_25_cbow_binary");
        System.out.println(embeddingAppleOrange.getSimilarity());

        System.out.println(System.currentTimeMillis());

        EmbeddingPair embeddingBananaStrawberry = EmbeddingSimConnection.EmbeddingSimPOST("banana", "strawberry", "_25_cbow_binary");
        System.out.println(embeddingBananaStrawberry.getSimilarity());

        System.out.println(System.currentTimeMillis());
    }

    public static void test1() {
        System.out.println("test1 start");

        String stringA;
        String stringB;

        stringA = "man";
        stringB = "woman";
        System.out.println("similarity('" + stringA + "', '" + stringB + "')");
        for (int t=0; t<100; t++) {
            EmbeddingSimConnection.EmbeddingSimGET(stringA, stringB, "_25_cbow_binary").getSimilarity();
            System.out.println(System.currentTimeMillis());
        }
        System.out.println();

        stringA = "king";
        stringB = "queen";
        System.out.println("similarity('" + stringA + "', '" + stringB + "')");
        for (int t=0; t<100; t++) {
            EmbeddingSimConnection.EmbeddingSimGET(stringA, stringB, "_25_cbow_binary").getSimilarity();
            System.out.println(System.currentTimeMillis());
        }
        System.out.println();

        stringA = "cat";
        stringB = "dog";
        System.out.println("similarity('" + stringA + "', '" + stringB + "')");
        for (int t=0; t<100; t++) {
            EmbeddingSimConnection.EmbeddingSimGET(stringA, stringB, "_25_cbow_binary").getSimilarity();
            System.out.println(System.currentTimeMillis());
        }
        System.out.println();
    }

    public static void test2() {
        System.out.println("test2 start");

        System.out.println(System.currentTimeMillis());

        for (int i=0; i<n; i+=2) {
            //System.out.println(word_data[i] + " * " + word_data[i+1] + ": ");
            EmbeddingPair embeddingPair = EmbeddingSimConnection.EmbeddingSimGET(word_data[i], word_data[i+1], "_25_cbow_binary");
            if (embeddingPair != null) {
                System.out.println(embeddingPair.getSimilarity());
            }
        }

        System.out.println(System.currentTimeMillis());
    }

    public static void test3() {
        System.out.println("test3 start");

        System.out.println(System.currentTimeMillis());

        for (int i=0; i<n; i+=2) {
            EmbeddingPair embeddingPair = EmbeddingSimConnection.EmbeddingSimGET(word_data[i], word_data[i+1], "_25_skip_binary");
            if (embeddingPair != null) {
                System.out.println(embeddingPair.getSimilarity());
            }
        }

        System.out.println(System.currentTimeMillis());
    }

    public static void test4() {
        System.out.println("test4 start");

        System.out.println(System.currentTimeMillis());

        for (int i=0; i<n; i+=2) {
            EmbeddingPair embeddingPair;
            embeddingPair = EmbeddingSimConnection.EmbeddingSimGET(word_data[i], word_data[i+1], "_25_cbow_binary");
            if (embeddingPair != null) {
                System.out.println(embeddingPair.getSimilarity());
            }
            embeddingPair = EmbeddingSimConnection.EmbeddingSimGET(word_data[i], word_data[i+1], "_25_skip_binary");
            if (embeddingPair != null) {
                System.out.println(embeddingPair.getSimilarity());
            }
        }

        System.out.println(System.currentTimeMillis());
    }

    public static void test5() {
        System.out.println("test5 start");

        System.out.println(System.currentTimeMillis());

        EmbeddingSim.SimilarityTechnique[] similarityTechniques = EmbeddingSim.SimilarityTechnique.values();
        EmbeddingPair embeddingPair;
        for (int st=0; st<similarityTechniques.length; st++) {
            for (int i=0; i<n; i+=2) {
                embeddingPair = EmbeddingSimConnection.EmbeddingSimGET(word_data[i], word_data[i+1], similarityTechniques[st].toString());
//                if (embeddingPair != null) {
//                    System.out.println(embeddingPair.getSimilarity());
//                }
            }
            System.out.println(System.currentTimeMillis());
        }

        System.out.println(System.currentTimeMillis());
    }


    public static void test6() {
        System.out.println("test6 start");

        System.out.println(System.currentTimeMillis());

        EmbeddingSim.SimilarityTechnique[] similarityTechniques = EmbeddingSim.SimilarityTechnique.values();
        EmbeddingPair embeddingPair;
        for (int i=0; i<n; i+=2) {
            for (int st=0; st<similarityTechniques.length; st++) {
                embeddingPair = EmbeddingSimConnection.EmbeddingSimGET(word_data[i], word_data[i+1], similarityTechniques[st].toString());
//                if (embeddingPair != null) {
//                    System.out.println(embeddingPair.getSimilarity());
//                }
            }
            System.out.println(System.currentTimeMillis());
        }

        System.out.println(System.currentTimeMillis());
    }
}
