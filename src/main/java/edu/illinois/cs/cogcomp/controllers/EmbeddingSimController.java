package edu.illinois.cs.cogcomp.controllers;

import edu.illinois.cs.cogcomp.embedding_sim.EmbeddingSim;
import edu.illinois.cs.cogcomp.models.EmbeddingPair;
import org.springframework.boot.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

//@RestController
@Controller
public class EmbeddingSimController {

    static HashMap<EmbeddingSim.SimilarityTechnique, EmbeddingSim> embeddingSimHashMap;

    static {
        embeddingSimHashMap = new HashMap<>();
        System.out.println(System.currentTimeMillis());

        EmbeddingSim.SimilarityTechnique[] similarityTechniques = EmbeddingSim.SimilarityTechnique.values();
        for (int st=0; st<similarityTechniques.length; st++) {
//            if (st == 2) {
//                break;//break for demo
//            }

            System.out.println(similarityTechniques[st]);
            embeddingSimHashMap.put(similarityTechniques[st], new EmbeddingSim(similarityTechniques[st]));//was already filled before timing
            System.out.println(System.currentTimeMillis());
        }

    }

    @RequestMapping(value = "/")
    String index(Model model) {
        return "index";
    }

    @RequestMapping(value = "/embedding_sim/{stringA}/{stringB}/{similarityTechnique}")
    public ResponseEntity<EmbeddingPair> embeddingSim(@PathVariable("stringA") String stringA, @PathVariable("stringB") String stringB, @PathVariable("similarityTechnique") String similarityTechnique) {
        EmbeddingPair embeddingPair = new EmbeddingPair();
        embeddingPair.setStringA(stringA);
        embeddingPair.setStringB(stringB);
        embeddingPair.setSimilarityTechnique(similarityTechnique);

        embeddingPairSetSimilarity(embeddingPair);
        return new ResponseEntity<EmbeddingPair>(embeddingPair, HttpStatus.OK);
    }

    //This is actually a GET method.
    @RequestMapping(value = "/embedding_sim", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<EmbeddingPair> embeddingSim(@RequestBody EmbeddingPair embeddingPair) {
        if (embeddingPair.getSimilarityTechnique() != null && embeddingPair.getStringA() != null && embeddingPair.getStringB() != null) {
            embeddingPairSetSimilarity(embeddingPair);
            return new ResponseEntity<EmbeddingPair>(embeddingPair, HttpStatus.OK);
        } else {
            return new ResponseEntity<EmbeddingPair>(embeddingPair, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/embedding_sim_demo", method = RequestMethod.GET)
    public String embeddingSimForm(Model model) {
        model.addAttribute("embedding_pair", new EmbeddingPair());
        return "embedding_sim/embedding_sim_form";
    }

    @RequestMapping(value = "/embedding_sim_demo", method = RequestMethod.POST)
    public String embeddingSimResult(@ModelAttribute() EmbeddingPair embeddingPair, Model model) {
        embeddingPairSetSimilarity(embeddingPair);
        model.addAttribute("embedding_pair", embeddingPair);
        return "embedding_sim/embedding_sim_result";
    }

    public static void embeddingPairSetSimilarity(@ModelAttribute() EmbeddingPair embeddingPair) {
        EmbeddingSim.SimilarityTechnique similarityTechnique = EmbeddingSim.SimilarityTechnique.valueOf(embeddingPair.getSimilarityTechnique());
        EmbeddingSim embeddingSim = embeddingSimHashMap.get(similarityTechnique);
        if (embeddingSim != null) {
            embeddingPair.setSimilarity(embeddingSim.similarity(embeddingPair.getStringA(), embeddingPair.getStringB()));
        } else {
            embeddingPair.setSimilarity(0);
        }
    }

//    @RequestMapping(value = "/shutdown")
//    public void shutdown() {
//        SpringApplication.exit(Application.applicationContext);
//    }

    @RequestMapping(value = "/memory")
    public String memory(Model model) {
        Runtime runtime = Runtime.getRuntime();
        System.out.println("Max Memory: " + runtime.maxMemory());
        System.out.println("Allocated Memory: " + runtime.totalMemory());
        System.out.println("Free Memory: " + runtime.freeMemory());

        return "index";
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(EmbeddingSimController.class, args);
    }

}
