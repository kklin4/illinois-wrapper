package edu.illinois.cs.cogcomp.controllers;

import edu.illinois.cs.cogcomp.common.EmbeddingSimMath;
import edu.illinois.cs.cogcomp.embedding_sim.EmbeddingSim;

import java.io.BufferedReader;
import java.io.FileReader;

public class EmbeddingSimDatasetTests {

    public static void main(String[] args) {
        wordSimTest();
        simLexTest();

        System.out.println("done");

//        Word 1,Word 2,Human (mean)
//        0.7692344208621273
//        0.7198077310892113
//        word1	word2	POS	SimLex999	conc(w1)	conc(w2)	concQ	Assoc(USF)	SimAssoc333	SD(SimLex)
//        0.6665304932030811
//        0.6849420676793203
//        done
    }

    private static void wordSimTest() {
        System.out.println("WS353 Dataset Test");

        String filePath = "small_data/wordsim353.csv";
        int n = 353;
        String[] stringAs = new String[n];
        String[] stringBs = new String[n];
        Double[] similarities = new Double[n];
        Double[][] cosine_similarities = new Double[EmbeddingSim.SimilarityTechnique.values().length][n];

        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            //System.out.println(br.readLine());
            br.readLine();
            for (int i=0; i<n; i++) {
                String line = br.readLine();
                String[] lineSplit = line.split(",");
                stringAs[i] = lineSplit[0];
                stringBs[i] = lineSplit[1];
                similarities[i] = Double.parseDouble(lineSplit[2]);

                //System.out.println(stringAs[i] + ", " + stringBs[i] + ", " + similarities[i]);

                for (int st=0; st<EmbeddingSim.SimilarityTechnique.values().length; st++) {
                    cosine_similarities[st][i] = EmbeddingSimConnection.EmbeddingSimGET(stringAs[i], stringBs[i], EmbeddingSim.SimilarityTechnique.values()[st]).getSimilarity();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int st=0; st<EmbeddingSim.SimilarityTechnique.values().length; st++) {
            System.out.println("\t" + EmbeddingSim.SimilarityTechnique.values()[st].toString() + ": " + EmbeddingSimMath.spearmanCorrelation(similarities, cosine_similarities[st]));
        }
    }

    private static void simLexTest() {
        System.out.println("SL999 Dataset Test");

        String filePath = "small_data/SimLex-999.txt";
        int n = 999;
        String[] stringAs = new String[n];
        String[] stringBs = new String[n];
        Double[] similarities = new Double[n];
        Double[][] cosine_similarities = new Double[EmbeddingSim.SimilarityTechnique.values().length][n];

        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            //System.out.println(br.readLine());
            br.readLine();
            for (int i=0; i<n; i++) {
                String line = br.readLine();
                String[] lineSplit = line.split("\t");
                stringAs[i] = lineSplit[0];
                stringBs[i] = lineSplit[1];
                similarities[i] = Double.parseDouble(lineSplit[3]);

                //System.out.println(stringAs[i] + ", " + stringBs[i] + ", " + similarities[i]);

                for (int st=0; st<EmbeddingSim.SimilarityTechnique.values().length; st++) {
                    cosine_similarities[st][i] = EmbeddingSimConnection.EmbeddingSimGET(stringAs[i], stringBs[i], EmbeddingSim.SimilarityTechnique.values()[st]).getSimilarity();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int st=0; st< EmbeddingSim.SimilarityTechnique.values().length; st++) {
            System.out.println("\t" + EmbeddingSim.SimilarityTechnique.values()[st].toString() + ": " + EmbeddingSimMath.spearmanCorrelation(similarities, cosine_similarities[st]));
        }
    }


}
