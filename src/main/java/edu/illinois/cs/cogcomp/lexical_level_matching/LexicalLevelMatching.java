package edu.illinois.cs.cogcomp.lexical_level_matching;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.cogcomp.embedding_sim.EmbeddingSim;
//import edu.illinois.cs.cogcomp.align.Aligner;
import edu.illinois.cs.cogcomp.llm.align.WordListFilter;
//import edu.illinois.cs.cogcomp.llm.comparators.WordComparator;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;

import java.util.ArrayList;
import java.util.Arrays;
//import edu.illinois.cs.cogcomp.mrcs.comparators.Comparator;
//import edu.illinois.cs.cogcomp.mrcs.dataStructures.EntailmentResult;
//import edu.illinois.cs.cogcomp.mrcs.dataStructures.Alignment;

public class LexicalLevelMatching {

    static EmbeddingSim embeddingSimWNSIM;
    static EmbeddingSim embeddingSim25CBOW4;

    static IllinoisTokenizer tokenizer;
    //static Comparator< String, EntailmentResult> comparator;
    static WordListFilter filter;
    static ResourceManager rm_;
    //static Aligner< String, EntailmentResult > aligner;

    public static void main(String[] args) throws Exception {

        embeddingSimWNSIM = new EmbeddingSim(EmbeddingSim.SimilarityTechnique.WNSim);
        //embeddingSim25CBOW4 = new EmbeddingSim(EmbeddingSim.SimilarityTechnique._25_cbow_4);

        tokenizer = new IllinoisTokenizer();
        String configFile_ = "config/alternativeLlmConfig.txt";
        rm_ = new ResourceManager( configFile_ );
        //comparator = new WordComparator( rm_ );
        filter = new WordListFilter( rm_ );
        //aligner = new Aligner< String, EntailmentResult >( comparator, filter );

        //System.out.println(LLM("The quick brown fox jumped over the lazy dog.", "The slow maroon fox jumped over the lazy dog.", embeddingSimWNSIM));
        //System.out.println(LLM("quick brown fox jumped over lazy dog", "slow maroon fox jumped over lazy dog", embeddingSimWNSIM));
        //System.out.println(LLM("The quick brown fox jumped over the lazy dog.", "The slow maroon fox jumped over the lazy dog.", embeddingSim25CBOW4));
        //System.out.println(LLM("quick brown fox jumped over lazy dog", "slow maroon fox jumped over lazy dog", embeddingSim25CBOW4));

        System.out.println(LLM("Four queens named lawsuit.", "Three kings were named in in in in the lawsuit.", embeddingSimWNSIM));
        System.out.println(LLM("Of the three kings referred to by their last names, Atawanaba was the oldest.", "Three kings were named in the lawsuit.", embeddingSimWNSIM));
        System.out.println(LLM("two plus two", "two plus two", embeddingSimWNSIM));
    }

    public static double LLM(String stringA, String stringB, EmbeddingSim embeddingSim) throws Exception {
        //String[] s1 = stringA.split(" ");
        //String[] s2 = stringB.split(" ");

        Pair< String[], IntPair[] > tokensA = tokenizer.tokenizeSentence(stringA);
        String[] splitA = tokensA.getFirst();
        String testA = splitA[0];
        Pair< String[], IntPair[] > tokensB = tokenizer.tokenizeSentence(stringB);
        String[] splitB = tokensB.getFirst();

        //Alignment<String> alignment =  aligner.align(s1, s2);
        String[] filterA = filter.filter(splitA);
        String[] filterB = filter.filter(splitB);

        String[] s1 = removeNulls(filterA);
        String[] s2 = removeNulls(filterB);

        return LLM(s1, s2, embeddingSim);
    }

    private static String[] removeNulls(String[] strings) {
        ArrayList<String> cleanStrings = new ArrayList<>();
        for (int s=0; s<strings.length; s++) {
            if (strings[s] != null) {
                cleanStrings.add(strings[s]);
            }
        }
        return cleanStrings.toArray(new String[cleanStrings.size()]);
    }

    public static double LLM(String[] s1, String[] s2, EmbeddingSim embeddingSim) {
//        int fillerCount = 0;
        double sumSimilarity = 0.0;
        for (int vi=0; vi<s2.length; vi++) {
            String v = s2[vi];

            double maxSimilarityUV = Integer.MIN_VALUE;
            for (int ui=0; ui<s1.length; ui++) {
                String u = s1[ui];

                double similarityUV = embeddingSim.similarity(u,v);
                if (similarityUV > maxSimilarityUV) {
                    maxSimilarityUV = similarityUV;
                }
            }
            sumSimilarity += maxSimilarityUV;

//            if (embeddingSim.similarity(v,v) == 0) {//flair
//                fillerCount++;
//            }
        }

//        System.out.println("fillerCount == " + fillerCount);
//        if (s2.length - fillerCount > 0) {
//            return sumSimilarity/(s2.length - fillerCount);
//        } else {
//            return 0.0;
//        }
        return sumSimilarity/s2.length;
    }

}
