package edu.illinois.cs.cogcomp.common;

import java.util.Arrays;
import java.util.Comparator;

public class EmbeddingSimMath {

    public static void main(String[] args) {
        //https://statistics.laerd.com/statistical-guides/spearmans-rank-order-correlation-statistical-guide.php
        Double[] english = {56.0, 75.0, 45.0, 71.0, 61.0, 64.0, 58.0, 80.0, 76.0, 61.0};
        Double[] math = {66.0, 70.0, 40.0, 60.0, 65.0, 56.0, 59.0, 77.0, 67.0, 61.0};

        System.out.println(spearmanCorrelation(english, math));
    }

    //https://en.wikipedia.org/wiki/Spearman%27s_rank_correlation_coefficient
    public static double spearmanCorrelation(Double[] vectorA, Double[] vectorB) {
        return pearsonCorrelation(ranks(vectorA), ranks(vectorB));
    }

    public static Double[] ranks(Double[] vector) {
        Integer[] sortedIndex = indexSort(vector);
        Double[] sortedVector = sort(vector);

        Double[] sortedRanks = new Double[sortedVector.length];
        int index = 0;
        int index_last;
        double currentDouble;
        while (index < sortedVector.length) {
            index_last = index + 1;
            currentDouble = sortedVector[index];

            while (index_last < sortedVector.length && sortedVector[index_last] == currentDouble) {
                index_last++;
            }

            double currentRank = 0.5 * (index + index_last + 1);
            for (int i=index; i<index_last; i++) {
                sortedRanks[i] = currentRank;
            }

            index = index_last;
        }

        Double[] ranks = new Double[sortedRanks.length];
        for (int i=0; i<ranks.length; i++) {
            ranks[sortedIndex[i]] = sortedRanks[i];
        }
        return ranks;
    }

    public static Integer[] indexSort(Double[] vector) {
        ArrayIndexComparator arrayIndexComparator = new ArrayIndexComparator(vector);
        Integer[] indices = arrayIndexComparator.createIndexArray();
        Arrays.sort(indices, arrayIndexComparator);
        return indices;
    }

    public static Double[] sort(Double[] vector) {
        Double[] sorted_vector = vector.clone();
        Arrays.sort(sorted_vector, new Comparator<Double>() {
            @Override
            public int compare(Double o1, Double o2) {
                //System.out.println(new Double(o2) + ", " + new Double(o1) + ", " + o2.compareTo(o1));
                return o2.compareTo(o1);
            }
        });
        return sorted_vector;
    }

    //https://en.wikipedia.org/wiki/Pearson_product-moment_correlation_coefficient
    public static double pearsonCorrelation(Double[] vectorA, Double[] vectorB) {
        return covariance(vectorA, vectorB) / standard_deviation(vectorA) / standard_deviation(vectorB);
    }

    public static double standard_deviation(Double[] vector) {
        return Math.sqrt(variance(vector));
    }

    public static double variance(Double[] vector) {
        return covariance(vector, vector);
    }

    public static double covariance(Double[] vectorA, Double[] vectorB) {
        return mean(multiply(vectorA, vectorB)) - mean(vectorA) * mean(vectorB);
    }

    public static Double[] multiply(Double[] vectorA, Double[] vectorB) {
        Double[] vectorAB = null;
        if (vectorA.length == vectorB.length) {
            vectorAB = new Double[vectorA.length];
            for (int i=0; i<vectorAB.length; i++) {
                vectorAB[i] = vectorA[i] * vectorB[i];
            }
        }
        return vectorAB;
    }

    public static double mean(Double[] vector) {
        return sum(vector)/vector.length;
    }

    public static double sum(Double[] vector) {
        double sum = 0.0;
        for (int i=0; i<vector.length; i++) {
            sum += vector[i];
        }
        return sum;
    }

    public static double cosine_similarity(Double[] vectorA, Double[] vectorB) {
        return dot_product(vectorA, vectorB) / magnitude(vectorA) / magnitude(vectorB);
    }

    public static double dot_product(Double[] vectorA, Double[] vectorB) {
        double dot_product = 0.0;
        for (int i=0; i<vectorA.length; i++) {
            dot_product += vectorA[i] * vectorB[i];
        }
        return dot_product;    }

    public static double magnitude(Double[] vector) {
        double magnitude = 0.0;
        for (int i=0; i<vector.length; i++) {
            magnitude += vector[i] * vector[i];
        }
        return Math.sqrt(magnitude);
    }

    public static void print(Double[] vector) {
        for (int i=0; i<vector.length; i++) {
            System.out.print(vector[i]);
            System.out.print(", ");
        }
        System.out.println();
    }

    public static void print(Integer[] vector) {
        for (int i=0; i<vector.length; i++) {
            System.out.print(vector[i]);
            System.out.print(", ");
        }
        System.out.println();
    }

}
