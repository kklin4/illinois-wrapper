package edu.illinois.cs.cogcomp.common;

import java.util.Comparator;

//http://stackoverflow.com/questions/4859261/get-the-indices-of-an-array-after-sorting
public class ArrayIndexComparator implements Comparator<Integer>
{
    private final Double[] array;

    public ArrayIndexComparator(Double[] array)
    {
        this.array = array;
    }

    public Integer[] createIndexArray()
    {
        Integer[] indexes = new Integer[array.length];
        for (int i = 0; i < array.length; i++)
        {
            indexes[i] = i; // Autoboxing
        }
        return indexes;
    }

    @Override
    public int compare(Integer index1, Integer index2) {
        // Autounbox from Integer to int to use as array indexes
        //System.out.println(array[index2] + ", " + array[index1] + ", " + array[index2].compareTo(array[index1]));
        return array[index2].compareTo(array[index1]);
    }
}