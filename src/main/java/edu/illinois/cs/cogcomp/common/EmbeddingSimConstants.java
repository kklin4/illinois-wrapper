package edu.illinois.cs.cogcomp.common;

public class EmbeddingSimConstants {
    public static final String PARAGRAMPATHWS = "paragramPathWS";
    public static final String PARAGRAMPATHSL = "paragramPathSL";
    
    //public static final String EVALWORDPATHCBOW = "evalWordPathCBOW";

    public static final String EVALWORDPATH25CBOWBINARY = "evalWordPath25CBOWBinary";
    public static final String EVALWORDPATH25CBOW4 = "evalWordPath25CBOW4";
    public static final String EVALWORDPATH25CBOW6 = "evalWordPath25CBOW6";
    public static final String EVALWORDPATH25CBOW8 = "evalWordPath25CBOW8";
    public static final String EVALWORDPATH200CBOWBINARY = "evalWordPath200CBOWBinary";
    public static final String EVALWORDPATH200CBOW4 = "evalWordPath200CBOW4";
    public static final String EVALWORDPATH200CBOW6 = "evalWordPath200CBOW6";
    public static final String EVALWORDPATH200CBOW8 = "evalWordPath200CBOW8";
    
    //public static final String EVALWORDPATHSKIP = "evalWordPathSKIP";

    public static final String EVALWORDPATH25SKIPBINARY = "evalWordPath25SKIPBinary";
    public static final String EVALWORDPATH25SKIP4 = "evalWordPath25SKIP4";
    public static final String EVALWORDPATH25SKIP6 = "evalWordPath25SKIP6";
    public static final String EVALWORDPATH25SKIP8 = "evalWordPath25SKIP8";
    public static final String EVALWORDPATH200SKIPBINARY = "evalWordPath200SKIPBinary";
    public static final String EVALWORDPATH200SKIP4 = "evalWordPath200SKIP4";
    public static final String EVALWORDPATH200SKIP6 = "evalWordPath200SKIP6";
    public static final String EVALWORDPATH200SKIP8 = "evalWordPath200SKIP8";

    public static final String ENWIKITEXTPATH200 = "enWikiTextPath200";//part 1 and part 2
    public static final String ENWIKITEXTPATH200_1 = "enWikiTextPath200_1";//part 1
    public static final String ENWIKITEXTPATH200_2 = "enWikiTextPath200_2";//part 2
}

