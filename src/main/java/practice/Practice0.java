package practice;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.Set;

public class Practice0 {
    public static void main(String[] args) {
        System.out.println("Hello, World!");

        try {
            System.out.println(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        Properties properties = System.getProperties();
        Set propertiesKeySet = properties.keySet();
        for (Object key : propertiesKeySet)
            System.out.println("The value of " + key + " is "
                    + properties.getProperty((String) key) + ".");
    }
}
