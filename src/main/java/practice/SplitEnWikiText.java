package practice;

import edu.illinois.cs.cogcomp.common.EmbeddingSimConstants;
import edu.illinois.cs.cogcomp.config.EmbeddingSimConfigurator;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;

import java.io.*;

public class SplitEnWikiText {

    public static void main(String[] args) throws Exception {
        System.out.println("Start");

        ResourceManager resourceManager = new EmbeddingSimConfigurator().getDefaultConfig();

        String embeddingDataPath = resourceManager.getString(EmbeddingSimConstants.ENWIKITEXTPATH200);
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(embeddingDataPath);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String thisLine = null;
        int count = 0;//

        System.setOut(new PrintStream(new FileOutputStream("en_wiki_text_vivek200_1.txt")));
        while (count < 3616575/2 && (thisLine = bufferedReader.readLine()) != null) {
            System.out.println(thisLine);
            count++;
        }
        System.out.flush();

        System.setOut(new PrintStream(new FileOutputStream("en_wiki_text_vivek200_2.txt")));
        while ((thisLine = bufferedReader.readLine()) != null) {
            System.out.println(thisLine);
            count++;
        }
        System.out.flush();

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        System.out.println("Finish");
    }

}
