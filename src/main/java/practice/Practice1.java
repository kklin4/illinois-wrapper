package practice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
//import java.net.URI;
//import java.net.URL;

import edu.illinois.cs.cogcomp.common.EmbeddingSimConstants;
import edu.illinois.cs.cogcomp.config.EmbeddingSimConfigurator;
//import edu.illinois.cs.cogcomp.config.WNSimConfigurator;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
//import edu.mit.jwi.IRAMDictionary;
//import edu.mit.jwi.RAMDictionaryFromJar;


public class Practice1 {
	
	public static void main(String[] args) {
		System.out.println("Hello, World!");
		
		ResourceManager resourceManager = new EmbeddingSimConfigurator().getDefaultConfig();
		String embeddingDataPath = resourceManager.getString(EmbeddingSimConstants.ENWIKITEXTPATH200_2);
		System.out.println(embeddingDataPath);
		
        try {
    		//InputStream inputStream = ClassLoader.getSystemResourceAsStream("paragram-data-ws/paragram_300_ws353.txt");
        	InputStream inputStream = ClassLoader.getSystemResourceAsStream(embeddingDataPath);
    		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
    		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String thisLine = null;
			int count = 0;//
			while ((thisLine = bufferedReader.readLine()) != null/* && count < 20*/) {
				//System.out.println(thisLine);
				count++;
			}
			System.out.println(count);//1808287//1808288
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		RAMDictionaryFromJar ramDictionaryFromJar = new RAMDictionaryFromJar();
//		IRAMDictionary iRamDictionary = ramDictionaryFromJar.getRAMDictionaryFromJar(paragramDataPath);
//		try {
//			iRamDictionary.open();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		ClassLoader classLoader = practice.Practice1.class.getClassLoader();
//		File file = new File(classLoader.getResource("paragram-data-ws/paragram_300_ws353.txt").getFile());
//		try {
//			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
//	        String thisLine = null;
//            while ((thisLine = bufferedReader.readLine()) != null) {
//                System.out.println(thisLine);
//            }
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		System.out.println("Done");
	}
}
