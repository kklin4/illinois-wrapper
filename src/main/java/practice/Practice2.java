package practice;

import java.util.Map;

import edu.illinois.cs.cogcomp.bigdata.mapdb.MapDB;


public class Practice2 {
	public static void main(String[] args) {
		System.out.println("Hello, World!");
		
		String mapPath = "data/embeddingSimResponseCachePractice/";
		String mapName = "embedding_sim_cache_practice";
		Map<String, String> map = MapDB.newDefaultDb(mapPath, mapName).make().getHashMap(mapName);
		//System.out.println(map.size());
		
		int runCount;
		String runCountString = map.get("run_count");
		if (runCountString != null) {
			runCount = Integer.parseInt(runCountString);
		} else {
			runCount = 0;
		}
		runCount++;
		map.put("run_count", Integer.toString(runCount));
		
		//System.out.println(runCount);
		
		for (String key : map.keySet()) {
	        System.out.println(key + ", " + map.get(key));
	    	//fb.lookup(key);
	    }
		
		
		
		String mapPath2 = "data/embeddingSimResponseCachePractice/";
		String mapName2 = "embedding_sim_cache_practice2";
		Map<String, Double[]> map2 = MapDB.newDefaultDb(mapPath2, mapName2).make().getHashMap(mapName2);
		//System.out.println(map2.size());
		
		Double[] embedding = new Double[25];
		for (int i=0; i<25; i++) {
			embedding[i] = (double)(i*i);
		}
		System.out.println(embedding);
		map2.put("embedding", embedding);
		
		for (String key : map2.keySet()) {
			for (int i=0; i<map2.get(key).length; i++) {
				System.out.println(map2.get(key)[i]);
			}
		}
		
		System.out.println("Done");
	}
}
