package practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
@EnableAutoConfiguration
public class GreetingControllerOld {

    private static int nextId;
    private static Map<Integer, Greeting> greetingMap;

    private static Greeting save(Greeting greeting) {
        System.out.println("Save: " + greeting.getId() + ", " + greeting.getText());
        if(greetingMap == null) {
            greetingMap = new HashMap<Integer, Greeting>();
            nextId = 1;
        }

        if (greeting.getId() != 0) {//if update
            Greeting oldGreeting = greetingMap.get(greeting.getId());

            if (oldGreeting == null) {
                return null;//to indicate that there is a problem. E.g. they try updating a greeting that they think is there but that is not there.
            }

            greetingMap.remove(greeting.getId());
            greetingMap.put(greeting.getId(), greeting);
        } else {//if create
            greeting.setId(nextId);
            greetingMap.put(greeting.getId(), greeting);
            nextId += 1;
        }

        return greeting;
    }

    private static boolean delete(int id) {
        System.out.println("Delete: " + id);
        Greeting deletedGreeting = greetingMap.remove(id);
        if (deletedGreeting == null) {
            return false;
        } else {
            return true;
        }
    }

    static {
        Greeting g1 = new Greeting();
        g1.setText("Hello World!");
        save(g1);

        Greeting g2 = new Greeting();
        g2.setText("Hola Mundo!");
        save(g2);
    }



    @RequestMapping(value = "/greetings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Greeting>> getGreetings() {
        Collection<Greeting> greetings = greetingMap.values();
        return new ResponseEntity<Collection<Greeting>>(greetings, HttpStatus.OK);
    }

    @RequestMapping(value = "/greetings/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Greeting> getGreeting(@PathVariable("id") int id) {
        Greeting greeting = greetingMap.get(id);
        if (greeting == null) {
            return new ResponseEntity<Greeting>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Greeting>(greeting, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/greetings", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Greeting> postGreeting(@RequestBody Greeting greeting) {
        Greeting postedGreeting = save(greeting);
        return new ResponseEntity<Greeting>(postedGreeting, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/greetings", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Greeting> putGreeting(@RequestBody Greeting greeting) {
        Greeting putGreeting = save(greeting);
        if (putGreeting == null) {
            return new ResponseEntity<Greeting>(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<Greeting>(putGreeting, HttpStatus.CREATED);
        }
    }

    //TODO: figure out why I can't send a RequestBody
    //@RequestMapping(value = "/greetings/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/greetings/{id}", method = RequestMethod.DELETE)
    //public ResponseEntity<Greeting> deleteGreeting(@PathVariable("id") int id, @RequestBody Greeting greeting) {
    public ResponseEntity<Greeting> deleteGreeting(@PathVariable("id") int id) {
        boolean deleted = delete(id);
        if (!deleted) {
            return new ResponseEntity<Greeting>(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<Greeting>(HttpStatus.NO_CONTENT);
        }
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(GreetingControllerOld.class, args);
    }

}
